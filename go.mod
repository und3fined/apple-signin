module applesignin

go 1.13

require (
	github.com/apex/log v1.1.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fasthttp/router v0.5.1
	github.com/gin-gonic/gin v1.4.0
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/golang/protobuf v1.3.1
	github.com/google/uuid v1.1.1
	github.com/klauspost/compress v1.8.5 // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/ugorji/go/codec v1.1.7
	github.com/valyala/fasthttp v1.5.0
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.2.0 // indirect
	go.uber.org/zap v1.10.0
	gopkg.in/go-playground/validator.v9 v9.30.0
	gopkg.in/yaml.v2 v2.2.2
)
