package main

import (
	"applesignin/src/server"
	"log"
)

func main() {
	log.Println("Server starting...")
	if err := server.New(); err != nil {
		log.Fatal(err)
	}
	log.Println("Server is started")
}
