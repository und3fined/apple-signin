package dir

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

// Root will return full path
// it very easy control generated file from code
func Root() string {
	exec, _ := os.Executable()
	execPath := filepath.Dir(exec)

	if strings.Contains(execPath, "/go-build") == true || strings.Contains(execPath, "/src/") {
		pwd, _ := os.Getwd()

		if strings.Contains(pwd, "/src") == true {
			var fixedPath string

			pwdSplit := strings.Split(pwd, "/")
			pwdSplit = reverse(pwdSplit)

			for _, item := range pwdSplit {
				fixedPath = fmt.Sprintf("%s../", fixedPath)

				if item == "src" {
					break
				}
			}
			pwd = filepath.Join(pwd, fixedPath)

			log.Printf("pwd: %s\n", pwd)
		}

		return pwd
	}

	if strings.Contains(execPath, "/bin") == true {
		return filepath.Join(execPath, "../")
	}

	return execPath
}

// Filepath return absolute file path
func Filepath(fileName string) string {
	root := Root()
	return filepath.Join(root, fileName)
}

func reverse(items []string) []string {
	reversed := []string{}

	for i := range items {
		n := items[len(items)-1-i]
		reversed = append(reversed, n)
	}

	return reversed
}
