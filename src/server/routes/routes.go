package routes

import (
	"github.com/fasthttp/router"
)

// New func
func New() *router.Router {
	r := router.New()
	r = registerHome(r)
	r = registerCallback(r)

	return r
}
