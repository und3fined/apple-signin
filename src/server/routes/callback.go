package routes

import (
	"applesignin/src/server/controller"

	"github.com/fasthttp/router"
)

// NewCallback func
func registerCallback(r *router.Router) *router.Router {
	callbackCtrl := controller.NewCallback()

	callbackRoute := r.Group("/callback")
	callbackRoute.POST("/appleid", callbackCtrl.AppleID)

	return r
}
