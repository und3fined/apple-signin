package routes

import (
	"github.com/fasthttp/router"

	"applesignin/src/server/controller"
)

func registerHome(r *router.Router) *router.Router {
	homeCtrl := controller.NewHome()

	r.GET("/", homeCtrl.Index)

	return r
}
