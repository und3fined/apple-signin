package controller

import (
	"html/template"
	"io/ioutil"
	"log"

	"github.com/valyala/fasthttp"

	"applesignin/src/server/utils"
	"applesignin/src/utils/dir"
)

// HomeCtrl struct
type HomeCtrl struct {
}

// Index page
func (home *HomeCtrl) Index(ctx *fasthttp.RequestCtx) {
	templatePath := dir.Filepath("./src/server/templates/index.tmpl")
	tplBytes, _ := ioutil.ReadFile(templatePath)

	authorizeURL, state := utils.AppleAuth.GenerateAuthorizeURL([]string{"name email"})

	tpl, err := template.New("index").Parse(string(tplBytes))
	if err != nil {
		log.Println(err)
	}

	tpl.Execute(ctx.Response.BodyWriter(), map[string]string{
		"authorize_url": authorizeURL,
		"state":         state,
	})
	ctx.Response.Header.Set("Content-Type", "text/html")
}

// NewHome Ctrl
func NewHome() *HomeCtrl {
	return &HomeCtrl{}
}
