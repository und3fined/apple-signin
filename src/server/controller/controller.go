package controller

import (
	"github.com/valyala/fasthttp"
)

// Controller interface
type Controller interface {
	Index(ctx *fasthttp.RequestCtx)
}
