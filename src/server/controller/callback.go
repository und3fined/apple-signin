package controller

import (
	"encoding/json"

	"github.com/valyala/fasthttp"

	"applesignin/src/apple-signin"
	"applesignin/src/server/utils"
)

const allowedContentType = "application/x-www-form-urlencoded"

// CallbackCtrl struct
type CallbackCtrl struct {
}

// AppleID receive data from apple
func (callback *CallbackCtrl) AppleID(ctx *fasthttp.RequestCtx) {
	if string(ctx.Request.Header.ContentType()) != allowedContentType {
		ctx.Error("Content-Type is not supported", fasthttp.StatusBadRequest)
		return
	}

	requestBody := ctx.Request.PostArgs()
	var authorizedResponse = apple.AuthorizationResponse{}

	requestBody.VisitAll(func(key, value []byte) {
		switch string(key) {
		case "state":
			authorizedResponse.State = string(value)
		case "code":
			authorizedResponse.Code = string(value)
		case "user":
			var user apple.User
			json.Unmarshal(value, &user)
			authorizedResponse.User = user
		case "id_token":
			authorizedResponse.IDToken = string(value)
		case "error":
			authorizedResponse.Error = string(value)
		}
	})

	if authorizedResponse.Error != "" {
		ctx.SetBody([]byte(authorizedResponse.Error))
		return
	}

	tokenResponse, err := utils.AppleAuth.Validate(authorizedResponse.Code)
	if err != nil {
		ctx.SetBody([]byte(err.Error()))
		return
	}

	userInfo, err := utils.AppleAuth.GetUser(tokenResponse.IDToken)
	if err != nil {
		ctx.SetBody([]byte(err.Error()))
		return
	}

	tokenResponse.IDToken = "" // remove IDToken

	if err != nil {
		ctx.SetBody([]byte(err.Error()))
		return
	}

	var response = make(map[string]interface{})
	response["authorized_response"] = authorizedResponse
	response["token_response"] = tokenResponse
	response["user_info"] = userInfo

	ctx.Response.Header.SetCanonical([]byte("Content-Type"), []byte("application/json"))
	ctx.Response.SetStatusCode(200)

	if err := json.NewEncoder(ctx).Encode(response); err != nil {
		ctx.Error(err.Error(), fasthttp.StatusInternalServerError)
	}
}

// NewCallback ctrl
func NewCallback() *CallbackCtrl {
	return &CallbackCtrl{}
}
