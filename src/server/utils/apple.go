package utils

import (
	"applesignin/src/apple-signin"
	"applesignin/src/utils/dir"
)

// AppleAuth variable
var AppleAuth *apple.Auth

func init() {
	AppleAuth = apple.NewAuth(
		apple.ClientID("vn.epic.service.auth.dev"),
		apple.KeyID("39RZH6RN93"),
		apple.TeamID("V6H56DA7YL"),
		apple.AuthKeyPath(dir.Filepath("./keys/AuthKey_39RZH6RN93.p8")),
		apple.RedirectURI("https://beta.oauth.moneylover.me/callback/appleid"),
	)

}
