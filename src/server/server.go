package server

import (
	"applesignin/src/server/routes"

	"github.com/valyala/fasthttp"
)

// New func
func New() error {
	router := routes.New()
	return fasthttp.ListenAndServe(":8080", router.Handler)
}
