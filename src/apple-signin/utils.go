package apple

import (
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"io/ioutil"
	"reflect"
	"strconv"
	"strings"

	"github.com/google/uuid"
)

// UserAgent generate a userAgent for request
func UserAgent() string {
	return "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.2 Safari/605.1.15"
}

// ID random from uuid
func ID() string {
	uid, _ := uuid.NewRandom()
	return strings.ToUpper(strings.Split(uid.String(), "-")[0])
}

// AuthKeyFromFile loads a .p8 certificate from a local file and returns a
// *ecdsa.PrivateKey.
func AuthKeyFromFile(filename string) (*ecdsa.PrivateKey, error) {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return AuthKeyFromBytes(bytes)
}

// AuthKeyFromBytes loads a .p8 certificate from an in memory byte array and
// returns an *ecdsa.PrivateKey.
func AuthKeyFromBytes(bytes []byte) (*ecdsa.PrivateKey, error) {
	block, _ := pem.Decode(bytes)
	if block == nil {
		return nil, errors.New("token: AuthKey must be a valid .p8 pem file")
	}
	key, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	switch pk := key.(type) {
	case *ecdsa.PrivateKey:
		return pk, nil
	default:
		return nil, errors.New("token: AuthKey must be of type ecdsa.PrivateKey")
	}
}

func convertToBool(boolStr interface{}) (val bool) {
	switch reflect.TypeOf(boolStr).String() {
	case "string":
		val, _ = strconv.ParseBool(boolStr.(string))
	default:
		val = boolStr.(bool)
	}

	return
}
