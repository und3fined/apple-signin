package apple

import jwt "github.com/dgrijalva/jwt-go"

// JWKSet - A set of JSON Web Key objects.
type JWKSet struct {
	Keys []JWKSetKey `json:"keys"` // An array that contains JSON Web Key objects.
}

// JWKSetKey - An object that defines a single JSON Web Key.
type JWKSetKey struct {
	Algorithm string `json:"alg,omitempty"` // The encryption algorithm used to encrypt the token.
	Exponent  string `json:"e,omitempty"`   // The exponent value for the RSA public key.
	KeyID     string `json:"kid,omitempty"` // A 10-character identifier key, obtained from your developer account.
	KeyType   string `json:"kty,omitempty"` // The key type parameter setting. This must be set to "RSA".
	Modulus   string `json:"n,omitempty"`   // The modulus value for the RSA public key.
	Use       string `json:"use,omitempty"` // The intended use for the public key.
}

// TokenResponse - The response token object returned on a successful request.
type TokenResponse struct {
	AccessToken  string `json:"access_token,omitempty"`  // (Reserved for future use) A token used to access allowed data. Currently, no data set has been defined for access.
	ExpiresIn    int64  `json:"expires_in,omitempty"`    // The amount of time, in seconds, before the access token expires.
	IDToken      string `json:"id_token,omitempty"`      // A JSON Web Token that contains the user’s identity information.
	RefreshToken string `json:"refresh_token,omitempty"` // The refresh token used to regenerate new access tokens. Store this token securely on your server.
	TokenType    string `json:"token_type,omitempty"`    // The type of access token. It will always be bearer.
	Error        string `json:"error,omitempty"`
}

// ErrorResponse - The error object returned after an unsuccessful request.
// A string that describes the reason for the unsuccessful request. The string consists of a single allowed value.
// Possible values: invalid_request, invalid_client, invalid_grant, unauthorized_client, unsupported_grant_type, invalid_scope
// The error field contains exactly one of the following parameters:
// invalid_request
// The request is malformed, normally due to a missing parameter, contains an unsupported parameter, includes multiple credentials, or uses more than one mechanism for authenticating the client.
// invalid_client
// The client authentication failed.
// invalid_grant
// The authorization grant or refresh token is invalid.
// unauthorized_client
// The client is not authorized to use this authorization grant type.
// unsupported_grant_type
// The authenticated client is not authorized to use the grant type.
// invalid_scope
// The requested scope is invalid.
type ErrorResponse struct {
	Error string `json:"error,omitempty"`
}

// IdentityToken - A JSON Web Token that contains the user’s identity information.
//
// Contains the following claims:
// iss - The issuer-registered claim key, which has the value https://appleid.apple.com.
// sub - The unique identifier for the user.
// aud - Your client_id in your Apple Developer account.
// exp - The expiry time for the token. This value is typically set to 5 minutes.
// iat - The time the token was issued.
// nonce - A String value used to associate a client session and an ID token.
//         This value is used to mitigate replay attacks and is present only
//         if passed during the authorization request.
// email - The user's email address.
// email_verified - A Boolean value that indicates whether the service
//                  has verified the email. The value of this claim is always true
//                  because the servers only return verified email addresses.
type IdentityToken struct {
	Nonce          string `json:"nonce,omitempty"`          // A String value used to associate a client session and an ID token. This value is used to mitigate replay attacks and is present only if passed during the authorization request.
	Email          string `json:"email,omitempty"`          // The user's email address.
	EmailVerified  bool   `json:"email_verified,omitempty"` // The value of this claim is always true because the servers only return verified email addresses.
	IsPrivateEmail bool   `json:"is_private_email,omitempty"`
	jwt.StandardClaims
}

// AuthorizationResponse - After the user clicks the Sign In with Apple button,
// the authorization information is sent to Apple.
type AuthorizationResponse struct {
	Code    string `json:"code,omitempty" form:"code"` // A single-use authentication code that is valid for five minutes.
	IDToken string `json:"id_token,omitempty" form:"id_token"`
	State   string `json:"state,omitempty" form:"state"`
	User    User   `json:"user,omitempty" form:"user"`
	Error   string `json:"error,omitempty" form:"error"`
}

// User struct
type User struct {
	Name  UserName `json:"name,omitempty"`
	Email string   `json:"email,omitempty"`
}

// UserName struct
type UserName struct {
	FirstName string `json:"firstName,omitempty"`
	LastName  string `json:"lastName,omitempty"`
}
