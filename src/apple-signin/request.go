package apple

import (
	"fmt"
	"time"

	"github.com/valyala/fasthttp"
)

// Request struct
type Request struct {
	request   *fasthttp.Request
	response  *fasthttp.Response
	userAgent string
	timeout   time.Duration
}

// SetTimeout func
// @params time int ak secondary
func (req *Request) SetTimeout(duration int64) {
	req.timeout = time.Duration(duration) * time.Second
}

// Post func
func (req *Request) Post(endpoint string, body string) (bodyBytes []byte, err error) {
	err = req.newRequest(fasthttp.MethodPost, endpoint)
	defer req.release()

	if err != nil {
		return
	}

	req.request.SetBodyString(body)
	err = fasthttp.DoTimeout(req.request, req.response, req.timeout)
	if err != nil {
		return
	}

	bodyBytes = req.response.Body()
	return
}

// Get func
func (req *Request) Get(endpoint string) (bodyBytes []byte, err error) {
	err = req.newRequest(fasthttp.MethodGet, endpoint)
	defer req.release()

	if err != nil {
		return
	}

	err = fasthttp.DoTimeout(req.request, req.response, req.timeout)
	if err != nil {
		return
	}

	bodyBytes = req.response.Body()

	if req.response.StatusCode() != 200 {
		err = fmt.Errorf("status code is invalid %d", req.response.StatusCode())
		return
	}

	return
}

func (req *Request) newRequest(method string, endpoint string) (err error) {
	request := fasthttp.AcquireRequest()
	response := fasthttp.AcquireResponse()

	request.SetRequestURI(endpoint)
	request.Header.SetMethod(method)
	request.Header.Add("User-Agent", req.userAgent)

	if method == fasthttp.MethodPost {
		request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	}

	req.request = request
	req.response = response
	return nil
}

func (req *Request) release() {
	fasthttp.ReleaseResponse(req.response)
	fasthttp.ReleaseRequest(req.request)
}

// NewRequest func
func NewRequest() *Request {
	userAgent := UserAgent()
	timeout := 10 * time.Second
	return &Request{userAgent: userAgent, timeout: timeout}
}
