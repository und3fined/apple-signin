package apple

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"strings"
	"sync"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

const secretTimeout = 15777000 // = 6 months in seconds

// Auth struct
type Auth struct {
	sync.Mutex

	opts  AuthOptions
	state string // state

	secretIssuedAt int64
	clientSecret   string
}

// GenerateAuthorizeURL func
func (auth *Auth) GenerateAuthorizeURL(scope []string) (loginURL string, state string) {
	state = ID()
	u, _ := url.Parse(AuthorizeEndpoint)
	query := u.Query()

	query.Set("response_type", "code")
	query.Set("response_mode", "form_post")
	query.Set("client_id", auth.opts.clientID)
	query.Set("redirect_uri", auth.opts.redirectURI)
	query.Set("scope", strings.Join(scope, " "))
	query.Set("state", state)
	u.RawQuery = query.Encode()
	loginURL = u.String()
	return
}

// Validate func
func (auth *Auth) Validate(code string) (TokenResponse, error) {
	auth.generateClientSecretIfExpired()

	reqBody := url.Values{}
	reqBody.Set("grant_type", "authorization_code")
	reqBody.Set("code", code)
	reqBody.Set("redirect_uri", auth.opts.redirectURI)
	reqBody.Set("client_id", auth.opts.clientID)
	reqBody.Set("client_secret", auth.opts.clientSecret)

	body := reqBody.Encode()
	req := NewRequest()

	bodyBytes, err := req.Post(TokenEndpoint, body)

	var tokenResp TokenResponse
	if err = json.Unmarshal(bodyBytes, &tokenResp); err != nil {
		return TokenResponse{}, err
	}

	if tokenResp.Error != "" {
		return TokenResponse{}, fmt.Errorf("validate token error: %s", tokenResp.Error)
	}

	return tokenResp, nil
}

// GetUser info
func (auth *Auth) GetUser(token string) (user IdentityToken, err error) {
	jwkSetKey, err := auth.fetchPublicKey()
	if err != nil {
		return
	}

	jwtToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %s", token.Header["alg"])
		}

		return genratePublicKey(jwkSetKey)
	})

	if claims, ok := jwtToken.Claims.(jwt.MapClaims); ok && jwtToken.Valid {
		user = IdentityToken{}

		// fuk apple document
		// see more in: https://developer.apple.com/documentation/signinwithapplerestapi/authenticating_users_with_sign_in_with_apple#3383773
		if emailVerified, ok := claims["email_verified"]; ok == true {
			claims["email_verified"] = convertToBool(emailVerified)
		}
		if isPrivateEmail, ok := claims["is_private_email"]; ok == true {
			claims["is_private_email"] = convertToBool(isPrivateEmail)
		}

		claimBytes, _ := json.Marshal(claims)
		err = json.Unmarshal(claimBytes, &user)
		return
	}

	if err != nil {
		return
	}

	if jwtToken.Valid {
		return
	}

	err = fmt.Errorf("unexpected claims method")
	return
}

// ClientSecret func get client secret
func (auth *Auth) ClientSecret() string {
	auth.generateClientSecretIfExpired()

	return auth.opts.clientSecret
}

// generates a new token.
func (auth *Auth) generateClientSecretIfExpired() {
	auth.Lock()
	defer auth.Unlock()
	if auth.secretExpired() {
		auth.generateClientSecret()
	}
}

// Expired checks to see if the token has expired.
func (auth *Auth) secretExpired() bool {
	return time.Now().Unix() >= (auth.secretIssuedAt + secretTimeout)
}

func (auth *Auth) generateClientSecret() error {
	issuedAt := time.Now().Unix()

	jwtToken := &jwt.Token{
		Header: map[string]interface{}{
			"alg": "ES256",
			"kid": auth.opts.keyID,
		},
		Claims: jwt.MapClaims{
			"iss": auth.opts.teamID,
			"iat": issuedAt,
			"exp": issuedAt + secretTimeout,
			"aud": "https://appleid.apple.com",
			"sub": auth.opts.clientID,
		},
		Method: jwt.SigningMethodES256,
	}

	clientSecret, err := jwtToken.SignedString(auth.opts.authKey)
	if err != nil {
		return err
	}

	auth.opts.clientSecret = clientSecret
	auth.secretIssuedAt = issuedAt

	return nil
}

func (auth *Auth) fetchPublicKey() (JWKSetKey, error) {
	req := NewRequest()
	bodyBytes, err := req.Get(PublicKeyEnpoint)

	if err != nil {
		return JWKSetKey{}, err
	}

	var jwkSet JWKSet
	if err = json.Unmarshal(bodyBytes, &jwkSet); err != nil {
		return JWKSetKey{}, err
	}

	return jwkSet.Keys[0], nil
}

// NewAuth func
func NewAuth(opts ...AuthOption) *Auth {
	var err error
	options := newOptions(opts...)
	options.authKey, err = AuthKeyFromFile(options.authKeyPath)

	if err != nil {
		log.Fatal(err)
	}

	return &Auth{opts: options}
}
