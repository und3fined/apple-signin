package apple

import "crypto/ecdsa"

// AuthOption type
type AuthOption func(*AuthOptions)

// AuthOptions struct
type AuthOptions struct {
	authKey      *ecdsa.PrivateKey
	authKeyPath  string // create new Key from https://developer.apple.com/account/resources/authkeys/list
	clientID     string
	clientSecret string
	keyID        string
	teamID       string
	redirectURI  string
}

func newOptions(opts ...AuthOption) AuthOptions {
	opt := AuthOptions{}

	for _, o := range opts {
		o(&opt)
	}

	return opt
}

// TeamID option
func TeamID(id string) AuthOption {
	return func(o *AuthOptions) {
		o.teamID = id
	}
}

// AuthKeyPath option
func AuthKeyPath(keyPath string) AuthOption {
	return func(o *AuthOptions) {
		o.authKeyPath = keyPath
	}
}

// ClientID option
func ClientID(id string) AuthOption {
	return func(o *AuthOptions) {
		o.clientID = id
	}
}

// KeyID option
func KeyID(id string) AuthOption {
	return func(o *AuthOptions) {
		o.keyID = id
	}
}

// RedirectURI option
func RedirectURI(uri string) AuthOption {
	return func(o *AuthOptions) {
		o.redirectURI = uri
	}
}
