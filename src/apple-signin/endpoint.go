package apple

// AuthorizeEndpoint - This endpoint use for generate login url
// TokenEndpoint - Validate the authorization grant code with Apple to obtain
//                 tokens or validate an existing refresh token.
// PublicKeyEnpoint - Fetch Apple’s public key to verify the ID token signature.
const (
	AuthorizeEndpoint = "https://appleid.apple.com/auth/authorize"
	TokenEndpoint     = "https://appleid.apple.com/auth/token"
	PublicKeyEnpoint  = "https://appleid.apple.com/auth/keys"
)
