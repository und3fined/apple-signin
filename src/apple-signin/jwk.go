package apple

import (
	"bytes"
	"crypto/rsa"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"math/big"
	"strings"
)

// GenratePublicKey func
func genratePublicKey(key JWKSetKey) (*rsa.PublicKey, error) {
	if key.KeyType == "" || key.Exponent == "" || key.Modulus == "" {
		return nil, fmt.Errorf("field required: kty, n and e")
	}

	switch key.KeyType {
	case "RSA":
		return createPemFromModulusAndExponent(key.Modulus, key.Exponent)
	default:
		return nil, fmt.Errorf("currenly only support RSA. your key type is %s", key.KeyType)
	}
}

func createPemFromModulusAndExponent(modulusRaw, exponentRaw string) (*rsa.PublicKey, error) {
	modulusBuf, err := urlsafeB64Decode(modulusRaw)
	if err != nil {
		return nil, fmt.Errorf("can't decode modulus. error: %s", err.Error())
	}
	exponentBuf, err := urlsafeB64Decode(exponentRaw)
	if err != nil {
		return nil, fmt.Errorf("can't decode exponent. error: %s", err.Error())
	}

	// initial modulus
	modulus := big.NewInt(0)
	modulus.SetBytes(modulusBuf)

	var exponentBytes []byte
	if len(exponentBuf) < 8 {
		exponentBytes = make([]byte, 8-len(exponentBuf), 8)
		exponentBytes = append(exponentBytes, exponentBuf...)
	} else {
		exponentBytes = exponentBuf
	}
	exponentReader := bytes.NewReader(exponentBytes)

	var exponent uint64
	err = binary.Read(exponentReader, binary.BigEndian, &exponent)
	if err != nil {
		return nil, fmt.Errorf("can't read exponent value")
	}

	return &rsa.PublicKey{N: modulus, E: int(exponent)}, nil
}

/**
* urlsafeB64Decode a string with URL-safe Base64.
* @param string input A Base64 encoded string
* @return string A decoded string
 */
func urlsafeB64Decode(input string) ([]byte, error) {
	remainder := len(input) % 4

	if remainder != 0 {
		padlen := 4 - remainder

		for padlen > 0 {
			input += "="
			padlen--
		}
	}

	input = strings.ReplaceAll(input, "-", "+")
	input = strings.ReplaceAll(input, "_", "/")

	return base64.StdEncoding.DecodeString(input)
}

func urlsafeB64Encode(input string) string {
	input = strings.ReplaceAll(input, "=", "")
	input = base64.StdEncoding.EncodeToString([]byte(input))
	input = strings.ReplaceAll(input, "+", "-")
	return strings.ReplaceAll(input, "/", "_")
}
