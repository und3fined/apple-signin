package apple

import (
	"applesignin/src/utils/dir"
	"testing"
)

func TestFetchKeys(t *testing.T) {
	auth := NewAuth(
		ClientID("vn.epic.service.auth.dev"),
		KeyID("39RZH6RN93"),
		TeamID("V6H56DA7YL"),
		AuthKeyPath(dir.Filepath("./keys/AuthKey_39RZH6RN93.p8")),
		RedirectURI("https://beta.oauth.moneylover.me/callback/appleid"),
	)

	token := "eyJraWQiOiJBSURPUEsxIiwiYWxnIjoiUlMyNTYifQ.eyJpc3MiOiJodHRwczovL2FwcGxlaWQuYXBwbGUuY29tIiwiYXVkIjoidm4uZXBpYy5zZXJ2aWNlLmF1dGguZGV2IiwiZXhwIjoxNTcwNDQ0NjkxLCJpYXQiOjE1NzA0NDQwOTEsInN1YiI6IjAwMDYyMi5hNDNjMDlkYmY1ZTY0OGVjYTZmYmU5N2FmYzJmZDllNC4wNTA0IiwiYXRfaGFzaCI6ImpOZWVXOURVZWZuQXdLVjBDNnphM2ciLCJhdXRoX3RpbWUiOjE1NzA0NDQwODl9.P8YvduAEyOM-bzjAODRpUW5NR-3yFIH-uO49WHb7YCnBM9tETCH4X8pcjvTEO9RwrHDbhG56jvN_FiEDZKclQUwGs8aweNcaXyvbS__Sm__7cEof-B5_gOwcUhKj-wlLizINRBUbl29ojh3rIKFzhmT-LVWqRL87vqufnT3GuPzUqUwo39sig_8EjHt65-u0PJXL_ZYyH7kzdiQFXAB-BVLVwjf4TtbqbRFwh3KX0syxKgl2Gxujv4oxXV07UlROixOVv24EUX1o4xplTwZh8689Uw-ojTkO5wbMyAO29pV88Y_exF0fG76u0bhTLLt1UA1AkNdxPBc_XLe2_d_11A"
	user, err := auth.GetUser(token)
	if err != nil {
		t.Error(err)
	}

	t.Log(user)
}
