# Apple Sign In

What the heck Sign in with Apple. The sample code for new Apple feature

## NOTE

It’s worth noting that Apple doesn’t allow localhost URLs in this step, and if you enter an IP address like 127.0.0.1 it will fail later in the flow. You have to use a real domain/server.

### Run

- Clone source to your server
- Run script with command `go run src/cmd/main.go`
- Use Nginx for reveser proxy domain to `localhost:8080`
